# Quickstart

In order to run this bot you will need to:
1. install  [cargo](https://doc.rust-lang.org/cargo/) on your system
2. create a telegram bot token using @BotFather
3. execute `TELOXIDE_TOKEN=<your telegram bot token> cargo run` in project directory
