extern crate reqwest;
extern crate teloxide;
extern crate log;

use teloxide::{
    prelude::*,
    utils::html::code_block,
    types::{
        ParseMode,
        File as TgFile
    },
    requests::Request,
    Bot
};
use std::sync::{Arc};
use tokio::task;

#[tokio::main]
async fn main() {
    run().await;
}

async fn run() {
    teloxide::enable_logging!();
    log::info!("Starting sticker_searcher bot!");
    let proxy = reqwest::Proxy::all("socks5://apcel.ml:443").unwrap().basic_auth("nobody", "nowhere");
    let client = reqwest::Client::builder()
        .proxy(proxy)
        .build()
        .unwrap();
    let bot = Bot::from_env_with_client(client);
    let dispatcher = Dispatcher::new(bot.clone());
    dispatcher
        .messages_handler(move |rx| async {
            handle_messages(bot, rx).await
        })
        .dispatch().await;
}

async fn download_file(bot: Arc<Bot>, file_id: String) -> Option<Vec<u8>> {
    let mut data = vec![];
    if let Ok(res) = bot.get_file(file_id).send().await {
        match res {
            TgFile { file_path, .. } => {
                if bot.download_file(&file_path, &mut data).await.is_ok() {
                    return Some(data);
                }
            }
        }
    }
    None
}

async fn handle_messages(bot: Arc<Bot>, mut rx: DispatcherHandlerRx<Message>) {
    while let Some(cx) = rx.recv().await {
        dbg!(&cx);
        if let teloxide::types::MessageKind::Common{ref media_kind, ..} = cx.update.kind {
            if let teloxide::types::MediaKind::Sticker{ref sticker} = media_kind {
                dbg!(sticker);
                let response = code_block(&format!("{:#?}", sticker));
                cx.answer(response).parse_mode(ParseMode::HTML).send().await.unwrap();
                let file_id = sticker.file_id.clone();
                let bot = bot.clone();
                task::spawn(async {
                    let data = download_file(bot, file_id).await.unwrap();
                    println!("{:?}", data);
                });
                continue;
            }
        }
        let response = "Not a sticker.";
        cx.answer(response).send().await.unwrap();
    }
}
